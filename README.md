# Digital Healthcare Information System

A system designed to manage healthcare data.

## Requirements

* Apache
* PHP version 7.3 or higher
	* [intl](http://php.net/manual/en/intl.requirements.php)
	* [libcurl](http://php.net/manual/en/curl.requirements.php) if you plan to use the HTTP\CURLRequest library
	* json (enabled by default - don't turn it off)
	* [mbstring](http://php.net/manual/en/mbstring.installation.php)
	* [mysqlnd](http://php.net/manual/en/mysqlnd.install.php)
	* xml (enabled by default - don't turn it off)
* MySQL version 5.1 or higher
* Git - https://git-scm.com/
* Brain

## Installation

* Download source code => ```git clone https://gitlab.com/chaorruptedx/digital-healthcare-system```
* In project directory, open a console terminal, execute the ```composer update``` command
* Import database from `database/digital_healthcare_system_v0.1.sql` into your MySQL
* Copy the `env` file to `.env`
* Open the `.env` file
	* Uncomment the line with ```CI_ENVIRONMENT```, then change ```production``` to ```development```
	* Update database configuration with real data and uncomment the required lines, for example:
```env
#--------------------------------------------------------------------
# ENVIRONMENT
#--------------------------------------------------------------------

CI_ENVIRONMENT = development

#--------------------------------------------------------------------
# DATABASE
#--------------------------------------------------------------------

database.default.hostname = localhost
database.default.database = digital_healthcare_system
database.default.username = root
database.default.password = mysql
database.default.DBDriver = MySQLi
database.default.DBPrefix =
```

## Running the System

* In project directory, open a console terminal, execute the ```php spark serve``` command
* The system can be view in the browser at http://localhost:8080

## Git Command

* To Pull System from GitLab:
	* ```git pull```

* To Push System to GitLab:
	* ```git pull```
	* ```git status```
	* ```git add YourFilePathwayName```
	* ```git status```
	* ```git commit -m "YourMessage"```
	* ```git pull```
	* ```git push```

## References

* Trello - https://trello.com/b/HRFjwWAz/digital-healthcare-system
* CodeIgniter 4.1.2 - https://codeigniter4.github.io/userguide/
* Bootstrap 5.0.1 - https://getbootstrap.com/docs/5.0/getting-started/introduction/
* Bootstrap Icons 1.5.0 - https://icons.getbootstrap.com/
* jQuery 3.6.0 - https://api.jquery.com/