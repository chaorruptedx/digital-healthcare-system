<?php

namespace Config;

use CodeIgniter\Config\BaseConfig;
use CodeIgniter\Filters\CSRF;
use CodeIgniter\Filters\DebugToolbar;
use CodeIgniter\Filters\Honeypot;
use App\Filters\GuestFilter;
use App\Filters\ReceptionistFilter;
use App\Filters\DoctorFilter;
use App\Filters\PharmacistFilter;
use App\Filters\PatientFilter;

class Filters extends BaseConfig
{
	/**
	 * Configures aliases for Filter classes to
	 * make reading things nicer and simpler.
	 *
	 * @var array
	 */
	public $aliases = [
		'csrf'     => CSRF::class,
		'toolbar'  => DebugToolbar::class,
		'honeypot' => Honeypot::class,
		'GuestFilter' => GuestFilter::class,
		'ReceptionistFilter' => ReceptionistFilter::class,
		'DoctorFilter' => DoctorFilter::class,
		'PharmacistFilter' => PharmacistFilter::class,
		'PatientFilter' => PatientFilter::class,
	];

	/**
	 * List of filter aliases that are always
	 * applied before and after every request.
	 *
	 * @var array
	 */
	public $globals = [
		'before' => [
			// 'honeypot',
			// 'csrf',
		],
		'after'  => [
			'toolbar',
			// 'honeypot',
		],
	];

	/**
	 * List of filter aliases that works on a
	 * particular HTTP method (GET, POST, etc.).
	 *
	 * Example:
	 * 'post' => ['csrf', 'throttle']
	 *
	 * @var array
	 */
	public $methods = [];

	/**
	 * List of filter aliases that should run on any
	 * before or after URI patterns.
	 *
	 * Example:
	 * 'isLoggedIn' => ['before' => ['account/*', 'profiles/*']]
	 *
	 * @var array
	 */
	public $filters = [
		'GuestFilter' => ['before' => ['/', 'about', 'login', 'register']],
		'ReceptionistFilter' => ['before' => ['receptionist/*']],
		'DoctorFilter' => ['before' => ['doctor/*']],
		'PharmacistFilter' => ['before' => ['pharmacist/*']],
		'PatientFilter' => ['before' => ['patient/*']],
	];
}
