<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('HomeController');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.


$routes->get('/', 'HomeController::index');
$routes->add('about', 'HomeController::about');
$routes->add('login', 'HomeController::login');
$routes->add('register', 'HomeController::register');
$routes->add('logout', 'HomeController::logout');

$routes->group('receptionist', function($routes)
{
	$routes->add('home', 'Receptionist\HomeController::index');
	
	$routes->add('Patient_Registration/List_of_patient', 'Receptionist\PatientRegistrationController::index');
	$routes->add('Patient_Registration/RegisterPatient', 'Receptionist\PatientRegistrationController::register_patient');
	$routes->add('Patient_Registration/ViewPatientDetails/(:num)', 'Receptionist\PatientRegistrationController::view_patient/$1');
	$routes->add('Patient_Registration/EditPatient/(:num)', 'Receptionist\PatientRegistrationController::edit_patient/$1');
	$routes->add('Patient_Registration/RemovePatient/(:num)', 'Receptionist\PatientRegistrationController::remove_patient/$1');
	$routes->add('Patient_Registration/AddPatientQueue/(:num)', 'Receptionist\PatientRegistrationController::add_patient_queue/$1');

	$routes->add('patient-queue/queue-list', 'Receptionist\PatientQueueController::index');

	$routes->add('Billing/List_of_bills', 'Receptionist\BillingController::index');
	$routes->add('Billing/BillingDetails/(:num)', 'Receptionist\BillingController::view_payment_details/$1');
	$routes->add('Billing/Paid/(:num)', 'Receptionist\BillingController::payment_paid/$1');

	$routes->add('logout', 'Receptionist\HomeController::logout');
});

$routes->group('doctor', function($routes)
{
	$routes->add('home', 'Doctor\HomeController::index');
	
	$routes->add('Profile/view_profile', 'Doctor\DoctorProfileController::index');
	$routes->add('Profile/create_profile', 'Doctor\DoctorProfileController::create_profile');
	$routes->add('Profile/edit_profile/(:num)', 'Doctor\DoctorProfileController::update_profile/$1');

	$routes->add('Consultation/PatientQueue', 'Doctor\ConsultationController::index');
	$routes->add('Consultation/PatientConsultation/(:num)', 'Doctor\ConsultationController::patient_consultation/$1');
	$routes->add('Consultation/MedicalHistory/(:num)', 'Doctor\ConsultationController::view_medical_history/$1');
	$routes->add('Consultation/SavePatientConsultation/(:num)', 'Doctor\ConsultationController::savePatientConsultation/$1');
	$routes->add('Medicine/make_order/(:num)', 'Doctor\ConsultationController::order_medicine/$1');
	$routes->add('Medicine/edit_order', 'Doctor\ConsultationController::edit_medicine_order');
	$routes->add('Medicine/remove_order/(:num)/(:num)', 'Doctor\ConsultationController::removeMedicineOrder/$1/$2');
	$routes->add('medicine/submit-order/(:num)/(:num)', 'Doctor\ConsultationController::submitMedicineOrder/$1/$2');
	

	$routes->add('logout', 'Doctor\HomeController::logout');
});

$routes->group('pharmacist', function($routes)
{
	$routes->add('home', 'Pharmacist\HomeController::index');
	
	$routes->add('Medicine/list_of_medicine', 'Pharmacist\MedicineManagementController::index');
	$routes->add('Medicine/register_medicine', 'Pharmacist\MedicineManagementController::add_medicine');
	$routes->add('Medicine/edit_medicine/(:num)', 'Pharmacist\MedicineManagementController::edit_medicine/$1');
	$routes->add('Medicine/remove_medicine/(:num)', 'Pharmacist\MedicineManagementController::remove_medicine/$1');

	$routes->add('Order/list_of_order', 'Pharmacist\MedicineOrderController::index');
	$routes->add('Order/view_order/(:num)', 'Pharmacist\MedicineOrderController::view_order_details/$1');
	$routes->add('Order/accept_order/(:num)', 'Pharmacist\MedicineOrderController::update_order_status/$1');

	$routes->add('logout', 'Pharmacist\HomeController::logout');
});

$routes->group('patient', function($routes)
{
	$routes->add('home', 'Patient\HomeController::index');
	$routes->add('logout', 'Patient\HomeController::logout');
});

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
