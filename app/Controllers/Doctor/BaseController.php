<?php

namespace App\Controllers\Doctor;

use CodeIgniter\Controller;
use CodeIgniter\HTTP\CLIRequest;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use App\Models\LookupMedicineModel;
use App\Models\UserModel;
use App\Models\PersonalDetailModel;
use App\Models\PatientQueueModel;
use App\Models\MedicalHistoryModel;
use App\Models\PharmacyMedicineModel;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 */

class BaseController extends Controller
{
	/**
	 * Instance of the main Request object.
	 *
	 * @var IncomingRequest|CLIRequest
	 */
	protected $request;

	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = [
		'gender_name_helper',
		'nav_helper',
		'dr_profile_helper'
	];

	/**
	 * Constructor.
	 *
	 * @param RequestInterface  $request
	 * @param ResponseInterface $response
	 * @param LoggerInterface   $logger
	 */
	public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		$this->session = \Config\Services::session();
		$this->lookup_medicine_model = new LookupMedicineModel();
		$this->user_model = new UserModel();
		$this->personal_detail_model = new PersonalDetailModel();
		$this->patient_queue_model = new PatientQueueModel();
		$this->medical_history_model = new MedicalHistoryModel();
		$this->pharmacy_medicine_model = new PharmacyMedicineModel();
	}
}
