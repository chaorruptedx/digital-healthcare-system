<?php

namespace App\Controllers\Doctor;

use App\Controllers\Doctor\BaseController;

use App\Models\PatientQueueModel;
use App\Models\MedicalHistoryModel;

class ConsultationController extends BaseController
{
	public function index()
	{
		$modelPatientQueues = $this->patient_queue_model->get();

		return view('main', [
			'title' => "List of Patient in Queue",
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('doctor/Consultation/PatientQueue', [
				'modelPatientQueues' => $modelPatientQueues,
			]),
		]);
	}

    public function patient_consultation($id_patient_queue)
	{
		$modelPatientQueue = $this->patient_queue_model->findByID($id_patient_queue);
		$modelMedicalHistory = $this->medical_history_model->findByID($modelPatientQueue['id_medical_history']);
		$modelPharmacyMedicines = $this->pharmacy_medicine_model->findCurrentOrder($modelPatientQueue['personal_detail_id']);
	
		return view('main', [
			'title' => "Patient Consultation",
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('doctor/Consultation/PatientConsultation', [
				'modelPatientQueue' => $modelPatientQueue,
				'modelMedicalHistory' => $modelMedicalHistory,
				'modelPharmacyMedicines' => $modelPharmacyMedicines,
			]),
		]);
	}

	public function savePatientConsultation($id_patient_queue)
	{
		$id_user_doctor = $this->session->get('id');

		$modelPersonalDetailDoctor = $this->personal_detail_model->findByIDUser($id_user_doctor);
		$modelPatientQueuePersonalDetail = $this->patient_queue_model->findPersonalDetailByIDPatientQueue($id_patient_queue);
		$modelPatientQueue = new PatientQueueModel();
		$modelMedicalHistory = new MedicalHistoryModel();

		if ($this->request->getMethod() === 'post')
		{
			$getPost = $this->request->getPost();

			$data_medical_history = [
				'id' => $modelPatientQueuePersonalDetail['id_medical_history'],
				'id_patient' => $modelPatientQueuePersonalDetail['personal_detail_id'],
				'id_doctor' => $modelPersonalDetailDoctor['id'],
				'height' => $getPost['height'],
				'weight' => $getPost['weight'],
				'blood_pressure' => $getPost['blood_pressure'],
				'notes' => $getPost['notes'],
			];

			$modelMedicalHistory->save($data_medical_history);
			$medical_history_insert_id = !empty($modelPatientQueuePersonalDetail['id_medical_history']) ? $modelPatientQueuePersonalDetail['id_medical_history'] : $modelMedicalHistory->InsertID();

			$data_patient_queue = [
				'id' => $id_patient_queue,
				'id_medical_history' => $medical_history_insert_id,
			];
			
			$modelPatientQueue->save($data_patient_queue);
		}

		return redirect()->to(base_url('doctor/Consultation/PatientConsultation/'.$id_patient_queue));
	}

    public function order_medicine($id_patient_queue)
	{
		$modelPatientQueue = $this->patient_queue_model->findByID($id_patient_queue);
		$modelLookupMedicines = $this->lookup_medicine_model->getMedicine();

		if ($this->request->getMethod() === 'post')
		{
			$getPost = $this->request->getPost();

			$this->pharmacy_medicine_model->add($modelPatientQueue['personal_detail_id'], $getPost); // Insert Medicine Order

			return redirect()->to(base_url('doctor/Consultation/PatientConsultation/'.$id_patient_queue));
		}

		return view('main', [
			'title' => "Medicine Order",
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('doctor/Medicine/make_order', [
				'id_patient_queue' => $id_patient_queue,
				'modelLookupMedicines' => $modelLookupMedicines,
			]),
		]);
	}

    public function edit_medicine_order()
	{
		return view('main', [
			'title' => "Edit Medicine Order",
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('doctor/Medicine/edit_order'),
		]);
	}

	public function view_medical_history($id_patient_queue)
	{
		$modelPatientQueuePersonalDetail = $this->patient_queue_model->findPersonalDetailByIDPatientQueue($id_patient_queue);
		$modelMedicalHistories = $this->medical_history_model->findByIDPatient($modelPatientQueuePersonalDetail['personal_detail_id']);

		return view('main', [
			'title' => "Patient Medical History",
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('doctor/Consultation/MedicalHistory', [
				'modelMedicalHistories' => $modelMedicalHistories,
			]),
		]);
	}

	public function removeMedicineOrder($id_pharmacy_medicine, $id_patient_queue)
	{
		$this->pharmacy_medicine_model->removeOrder($id_pharmacy_medicine);

		return redirect()->to(base_url('doctor/Consultation/PatientConsultation/'.$id_patient_queue));
	}

	public function submitMedicineOrder($id_personal_detail_patient, $id_patient_queue)
	{
		$this->pharmacy_medicine_model->submitOrder($id_personal_detail_patient);
		$this->patient_queue_model->endQueue($id_patient_queue);

		return redirect()->to(base_url('doctor/Consultation/PatientQueue/'));
	}
}
