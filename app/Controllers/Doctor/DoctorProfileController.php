<?php namespace App\Controllers\Doctor;

use App\Controllers\Doctor\BaseController;
use App\Models\PersonalDetailModel;

class DoctorProfileController extends BaseController
{
	public function index()
	{
		$id = $this->session->get('id');

		$PersonalDetailModel = new PersonalDetailModel();
		$doctor_profile = $PersonalDetailModel->getDoctorProfileByIDUser($id);

		//dd($doctor_profile);
		return view('main', [
			'title' => "Doctor Profile",
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('doctor/Profile/view_profile',[
				'doctor_profile' => $doctor_profile,
			]),
		]);
		
	}

	public function create_profile()
	{
		if ($this->request->getMethod() === 'post')
		{
			$id_user=$this->session->get('id');

			$doctor_profile = new PersonalDetailModel();

			$validation = $doctor_profile->createDoctorProfile($id_user,$this->request->getPost());
			
			if ($validation === true)
				$success = 'Doctor Profile successfully created.';
			else
				$errors = $validation;

			return redirect()->to(base_url('doctor/Profile/view_profile'));
		}
		
		return view('main', [
			'title' => "Create Doctor Profile",
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('doctor/Profile/create_profile'),
		]);
	}

	public function update_profile($id)
	{
		$PersonalDetailModel = new PersonalDetailModel();
		$doctor_profile = $PersonalDetailModel->getDoctorProfileByID($id);

		//dd($doctor_profile);
		
		if ($this->request->getMethod() === 'post')
		{	
			$doctor_profile = new PersonalDetailModel();
			
			$validation = $doctor_profile->editDoctorProfile($id, $this->request->getPost());

			if ($validation === true)
				$success = 'Doctor Profile successfully updated.';
			else
				$errors = $validation;

				$doctor_profile = $PersonalDetailModel->getDoctorProfileByID($id); //replace data lama dengan yg baru (updated)
		}

		return view('main', [
			'title' => "Update Doctor Profile",
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('doctor/Profile/edit_profile',[
				'doctor_profile' => $doctor_profile,
			]),
		]);
	}
}
