<?php

namespace App\Controllers\Doctor;

use App\Controllers\Doctor\BaseController;

class HomeController extends BaseController
{
	public function index()
	{
		return view('main', [
			'title' => null,
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('doctor/home'),
		]);
	}

	public function logout()
	{
        $this->session->destroy();
        
        return redirect()->to(base_url('/'));
	}
}
