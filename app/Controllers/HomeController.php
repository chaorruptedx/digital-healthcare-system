<?php

namespace App\Controllers;

class HomeController extends BaseController
{
	public function index()
	{
		return view('main', [
			'title' => null,
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('home'),
		]);
	}

	public function about()
	{
		return view('main', [
			'title' => 'About',
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('about'),
		]);
	}

	public function login()
	{
		if ($this->request->getMethod() === 'post')
		{
			$validation = $this->user_model->authenticateUser($this->request->getPost());
			
			if ($validation === true)
			{
				$userModel = $this->user_model->getUserByEmail($this->request->getPost('email'));
				
				$this->session->set([
					'id' => $userModel['id'],
					'email' => $userModel['email'],
					'role' => $userModel['role'],
				]);

				if ($this->session->get('role') == 1) // Receptionist
					return redirect()->to(base_url('/receptionist/home'));
				else if ($this->session->get('role') == 2) // Doctor
					return redirect()->to(base_url('/doctor/home'));
				else if ($this->session->get('role') == 3) // Pharmacist
					return redirect()->to(base_url('/pharmacist/home'));
				else if ($this->session->get('role') == 4) // Patient
					return redirect()->to(base_url('/patient/home'));
				else
					$errors[] = 'Error: Unknown role detected.';
			}
			else
			{
				$errors[] = 'Wrong email or password. Please try again.';
			}
		}

		return view('main', [
			'title' => 'Login',
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('login'),
		]);
	}

	public function register()
	{
		if ($this->request->getMethod() === 'post')
		{
			$validation = $this->user_model->registerUser($this->request->getPost());
			
			if ($validation === true)
				$success = 'User successfully registered into the system.';
			else
				$errors = $validation;
		}

		return view('main', [
			'title' => 'Register',
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('register'),
		]);
	}
}
