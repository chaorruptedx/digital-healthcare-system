<?php

namespace App\Controllers\Patient;

use App\Controllers\Patient\BaseController;

class HomeController extends BaseController
{
	public function index()
	{
		return view('main', [
			'title' => null,
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('patient/home'),
		]);
	}

	public function logout()
	{
        $this->session->destroy();
        
        return redirect()->to(base_url('/'));
	}
}
