<?php namespace App\Controllers\Pharmacist;

use App\Controllers\Pharmacist\BaseController;
use App\Models\LookupMedicineModel;

class MedicineManagementController extends BaseController
{
	public function index()
	{
        $medicine = new LookupMedicineModel();
		$data['medicine'] = $medicine->getMedicine();

		return view('main', [
			'title' => "Medicine Management",
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('pharmacist/Medicine/list_of_medicine',[
                'medicine' => $data['medicine']
            ]),
		]);
	}

	public function add_medicine()
	{
        if ($this->request->getMethod() === 'post')
		{
			$medicine = new LookupMedicineModel();

			$validation = $medicine->addMedicine($this->request->getPost());
			
			if ($validation === true)
				$success = 'Medicine successfully added into the system.';
			else
				$errors = $validation;
		}
		
		return view('main', [
			'title' => "Add Medicine",
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('pharmacist/Medicine/register_medicine'),
		]);
	}

	public function edit_medicine($id)
	{
		$medicine = new LookupMedicineModel();

		$medicine = $medicine->getMedicineByID($id);
		

		if ($this->request->getMethod() === 'post')
		{	
			$medicine = new LookupMedicineModel();
			
			$validation = $medicine->editMedicine($id, $this->request->getPost());
			
			if ($validation === true)
				$success = 'Medicine details successfully updated.';
			else
				$errors = $validation;

			$medicine = $medicine->getMedicineByID($id);
		}
		
		return view('main', [
			'title' => "Update Medicine",
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('pharmacist/Medicine/edit_medicine',[
				'medicine' => $medicine,
			]),
		]);
	}

	public function remove_medicine($id)
	{
		$validation = $this->lookup_medicine_model->removeMedicine($id);
			
			if ($validation === true)
				$success = 'Medicine has been removed successfully.';
			else
				$errors = $validation;
		
		return redirect()->to(base_url('pharmacist/Medicine/list_of_medicine'));
	}
}