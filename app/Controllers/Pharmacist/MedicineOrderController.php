<?php namespace App\Controllers\Pharmacist;

use App\Controllers\Pharmacist\BaseController;
use App\Models\LookupMedicineModel;

class MedicineOrderController extends BaseController
{
	public function index()
	{
		$modelPharmacyMedicines = $this->personal_detail_model->findAllSubmittedOrder();

		return view('main', [
			'title' => "List of Medicine Order",
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('pharmacist/Order/list_of_order', [
				'modelPharmacyMedicines' => $modelPharmacyMedicines,
			]),
		]);
	}

	public function view_order_details($id_patient)
	{
		$modelPersonalDetailPatient = $this->personal_detail_model->getPatientByID($id_patient);
		$modelPharmacyMedicines = $this->pharmacy_medicine_model->findSubmittedOrder($id_patient);

        return view('main', [
			'title' => "Order Details",
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('pharmacist/Order/view_order', [
				'modelPersonalDetailPatient' => $modelPersonalDetailPatient,
				'modelPharmacyMedicines' => $modelPharmacyMedicines,
			]),
		]);
	}

	public function update_order_status($id_patient)
	{
		$this->pharmacy_medicine_model->finishOrder($id_patient);

		return redirect()->to(base_url('pharmacist/Order/list_of_order'));
	}
}