<?php

namespace App\Controllers\Receptionist;

use App\Controllers\Receptionist\BaseController;

class BillingController extends BaseController
{
	public function index()
	{
		$modelPersonalDetailPatients = $this->personal_detail_model->findAllFinishhOrder();

		return view('main', [
			'title' => "List of Patient Bills",
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('Receptionist/Billing/List_of_bills', [
				'modelPersonalDetailPatients' => $modelPersonalDetailPatients,
			]),
		]);
	}

    public function view_payment_details($id_personal_detail)
	{
		$modelPersonalDetailPatient = $this->personal_detail_model->getPatientByID($id_personal_detail);
		$modelPharmacyMedicines = $this->pharmacy_medicine_model->findFinishOrder($id_personal_detail);

		return view('main', [
			'title' => "Payment Details",
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('Receptionist/Billing/BillingDetails', [
				'modelPersonalDetailPatient' => $modelPersonalDetailPatient,
				'modelPharmacyMedicines' => $modelPharmacyMedicines,
			]),
		]);
	}

	public function payment_paid($id_personal_detail)
	{
		$this->pharmacy_medicine_model->payOrder($id_personal_detail);

		return redirect()->to(base_url('receptionist/Billing/List_of_bills'));
	}
	
}
