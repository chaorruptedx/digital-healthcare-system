<?php

namespace App\Controllers\Receptionist;

use App\Controllers\Receptionist\BaseController;

class HomeController extends BaseController
{
	public function index()
	{
		return view('main', [
			'title' => "Dashboard",
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('receptionist/home'),
		]);
	}

	public function logout()
	{
        $this->session->destroy();
        
        return redirect()->to(base_url('/'));
	}
}
