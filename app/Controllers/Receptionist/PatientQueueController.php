<?php

namespace App\Controllers\Receptionist;

use App\Controllers\Receptionist\BaseController;

class PatientQueueController extends BaseController
{
	public function index()
	{
		$modelPatientQueues = $this->patient_queue_model->get();

		return view('main', [
			'title' => 'List of Patient Queue',
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('Receptionist/patient-queue/index', [
				'modelPatientQueues' => $modelPatientQueues,
			]),
		]);
	}
}
