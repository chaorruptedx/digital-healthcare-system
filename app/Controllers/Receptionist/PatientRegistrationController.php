<?php

namespace App\Controllers\Receptionist;

use App\Controllers\Receptionist\BaseController;

class PatientRegistrationController extends BaseController
{
	public function index()
	{
		$listPatient = $this->personal_detail_model->getPatient();

		return view('main', [
			'title' => "List Of Registered Patient",
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('Receptionist/Patient_Registration/List_of_patient', [
				'listPatient' => $listPatient,
			]),
		]);
	}

	public function register_patient()
	{
		$listState = $this->lookup_state_model->getState();

		if ($this->request->getMethod() === 'post')
		{
			$validation = $this->personal_detail_model->registerPatient($this->request->getPost());
			
			if ($validation === true)
				$success = 'Patient successfully registered into the system.';
			else
				$errors = $validation;
		}

		return view('main', [
			'title' => "Patient Registration",
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('Receptionist/Patient_Registration/RegisterPatient', [
				'listState' => $listState,
			]),
		]);
	}

	public function view_patient($id_personal_detail)
	{
		$patient_personal_detail_data = $this->personal_detail_model->getPatientByID($id_personal_detail);

		return view('main', [
			'title' => "Patient Information Details",
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('Receptionist/Patient_Registration/ViewPatientDetails', [
				'patient_personal_detail_data' => $patient_personal_detail_data,
			]),
		]);
	}

	public function edit_patient($id_personal_detail)
	{	
		if ($this->request->getMethod() === 'post')
		{
			$validation = $this->personal_detail_model->updatePatient($id_personal_detail, $this->request->getPost());
			
			if ($validation === true)
				$success = 'Patient details has been successfully updated.';
			else
				$errors = $validation;
		}

		$listState = $this->lookup_state_model->getState();
		$patient_personal_detail_data = $this->personal_detail_model->getPatientByID($id_personal_detail);

		return view('main', [
			'title' => "Edit Patient Information",
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('Receptionist/Patient_Registration/EditPatient', [
				'listState' => $listState,
				'patient_personal_detail_data' => $patient_personal_detail_data,
			]),
		]);
	}

	public function remove_patient($id_personal_detail)
	{
		$validation = $this->personal_detail_model->removePatient($id_personal_detail);
			
			if ($validation === true)
				$success = 'Patient has been removed successfully.';
			else
				$errors = $validation;
		
		return redirect()->to(base_url('receptionist/Patient_Registration/List_of_patient'));
	}
	
	public function add_patient_queue($id_personal_detail)
	{
		$validation = $this->patient_queue_model->add($id_personal_detail);
			
			if ($validation === true)
				$success = 'Patient has been added to the queue successfully.';
			else
				$errors = $validation;

		$listPatient = $this->personal_detail_model->getPatient();

		return view('main', [
			'title' => "List Of Registered Patient",
			'success' => (isset($success)) ? $success : null,
			'errors' => (isset($errors)) ? $errors : null,
			'content' => view('Receptionist/Patient_Registration/List_of_patient', [
				'listPatient' => $listPatient,
			]),
		]);	
	}
}
