<?php

namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class GuestFilter implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        $session = session();

        if ($session->has('id'))
        {
            if ($session->get('role') == 1)
            {
                return redirect()->to(base_url('receptionist/home'));
            }
            elseif ($session->get('role') == 2)
            {
                return redirect()->to(base_url('doctor/home'));
            }
            elseif ($session->get('role') == 3)
            {
                return redirect()->to(base_url('pharmacist/home'));
            }
            elseif ($session->get('role') == 4)
            {
                return redirect()->to(base_url('patient/home'));
            }
        }
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Do something here
    }
}