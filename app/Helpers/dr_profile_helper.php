<?php  
use App\Models\UserModel;

if ( ! function_exists('dr_profile'))
{
    function dr_profile($id_user)
    {
        $userModel = new UserModel();

        $exist_profile = $userModel->checkExistingDrProfile($id_user);

        if ($exist_profile)
            return true;
        else
            return false;
    }
}