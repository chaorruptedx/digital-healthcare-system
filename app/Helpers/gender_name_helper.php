<?php  
if (!function_exists('gender_name'))
{
    function gender_name($gender)
    {
        if ($gender == 1)
        {
            return 'Male';
        }
        else if ($gender == 2)
        {
            return 'Female';
        }
    }
}