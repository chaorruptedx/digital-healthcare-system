<?php namespace App\Models;

use CodeIgniter\Model;

class BankStatusHelper extends Model
{
    public static function getStateName($id)
    {
        $modelLookupState = new LookupStateModel();

        $state = $modelLookupState->find($id);

        return $state['name'];
    }
}