<?php namespace App\Models;

use CodeIgniter\Model;

class LookupMedicineModel extends Model
{
    protected $table = 'lookup_medicine';
    protected $primaryKey = 'id';
    protected $allowedFields = ['code','name', 'price', 'status'];

    protected $validationRules = [
        'name' => 'required|max_length[50]',
        'code' => 'required',
        'price' => 'required',
    ];

    protected $validationRulesForCreateUpdate = [
        'name' => 'required|max_length[50]',
        'code' => 'required',
        'price' => 'required',
    ];

    protected $validationRulesForUpdateSameName = [
        'name' => 'required|max_length[50]',
        'code' => 'required',
        'price' => 'required',
    ];

    public function getMedicine()
    {
        return $this->where('status', 1)
            ->findAll();
    }

    public function getMedicineByID($id)
    {
        return $this->find($id);
    }

    public function addMedicine($getPost)
    {   
        $data = [
            'code' => $getPost['code'],
            'name' => $getPost['name'],
            'price'  => $getPost['price'],
        ];

        if (!$this->checkUniqueName($getPost['name']))
            $this->setValidationRules($this->validationRulesForCreateUpdate);
        
        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function editMedicine($id, $getPost)
    {
        $data = [
            'id' => $id,
            'code' => $getPost['code'],
            'name' => $getPost['name'],
            'price'  => $getPost['price'],
        ];
        //dd($data);

        $medicine = $this->getMedicineByID($id);

        if (!$this->checkUniqueName($getPost['name']))
        $this->setValidationRules($this->validationRulesForUpdateSameName);
        
        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function removeMedicine($id)
    {   
        $data = [
            'id' => $id,
            'status' => -1,
        ];

        return $this->save($data);
    }

    public function checkUniqueName($name)
    {
        $check_unique_name = $this
            ->where([
                'name' => $name,
                'status' => 1,
            ])
            ->first();

        if (!$check_unique_name)
            return true;
        else
            return false;
    }
}