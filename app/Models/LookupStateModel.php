<?php namespace App\Models;

use CodeIgniter\Model;

class LookupStateModel extends Model
{
    protected $table = 'lookup_state';
    protected $primaryKey = 'id';
    protected $allowedFields = ['name', 'status'];

    public function getState()
    {
        return $this->where('status', 1)
            ->findAll();
    }
}