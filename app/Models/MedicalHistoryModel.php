<?php namespace App\Models;

use CodeIgniter\Model;

class MedicalHistoryModel extends Model
{
    protected $table = 'medical_history';
    protected $primaryKey = 'id';
    protected $allowedFields = ['id_patient', 'id_doctor', 'id_bill', 'height', 'weight', 'blood_pressure', 'notes', 'status'];

    public function findByID($id)
    {
        return $this->where([
            'medical_history.id' => $id,
        ])
        ->first();
    }

    public function findByIDPatient($id_patient)
    {
        return $this->where([
            'id_patient' => $id_patient,
            'status' => 1,
        ])
        ->findAll();
    }
}