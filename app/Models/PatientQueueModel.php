<?php namespace App\Models;

use CodeIgniter\Model;

class PatientQueueModel extends Model
{
    protected $table = 'patient_queue';
    protected $primaryKey = 'id';
    protected $allowedFields = ['id_patient', 'id_medical_history', 'queue_status', 'status'];

    public function add($id_personal_detail)
    {
        $data = [
            'id_patient' => $id_personal_detail,
            'queue_status' => 1,
        ];

        if ($this->save($data))
        {
            return true;
        }
        else
        {
            return $this->errors();
        }
    }

    public function get()
    {
        return $this->select([
                'patient_queue.id AS patient_queue_id',
                'patient_detail.name AS patient_detail_name',
                'patient_detail.nric_no AS patient_detail_nric_no',
                'patient_detail.created_at AS patient_detail_created_at'
            ])
            ->join('personal_detail patient_detail', 'patient_detail.id = patient_queue.id_patient')
            ->where([
                'patient_queue.queue_status' => 1,
                'patient_queue.status' => 1,
            ])
            ->where('patient_detail.id_user IS NULL', null, false) // Patient
            ->orderBy('patient_detail.created_at', 'DESC')
            ->findAll();
    }

    public function findByID($id_patient_queue)
    {
        return $this->select([
            'patient_queue.id_medical_history',
            'patient_queue.id AS patient_queue_id',
            'patient_detail.id AS personal_detail_id',
            'patient_detail.id_state AS patient_detail_id_state',
            'patient_detail.name AS patient_detail_name',
            'patient_detail.nric_no AS patient_detail_nric_no',
            'patient_detail.tel_no AS patient_detail_tel_no',
            'patient_detail.gender AS patient_detail_gender',
            'patient_detail.address AS patient_detail_address',
        ])
        ->join('personal_detail patient_detail', 'patient_detail.id = patient_queue.id_patient')
        ->where([
            'patient_queue.id' => $id_patient_queue,
        ])
        ->first();
    }

    public function findPersonalDetailByIDPatientQueue($id_patient_queue)
    {
        return $this->select([
            'patient_queue.id AS patient_queue_id',
            'patient_queue.id_medical_history',
            'personal_detail.id AS personal_detail_id',
        ])
        ->join('personal_detail', 'personal_detail.id = patient_queue.id_patient')
        ->where([
            'patient_queue.id' => $id_patient_queue,
        ])
        ->first();
    }

    public function endQueue($id)
    {
        $data = [
            'id' => $id,
            'queue_status' => 2, // End Queue
        ];

        $this->save($data);
    }
}