<?php namespace App\Models;

use CodeIgniter\Model;

class PersonalDetailModel extends Model
{
    protected $table = 'personal_detail';
    protected $primaryKey = 'id';
    protected $allowedFields = ['id_user', 'id_state', 'nric_no', 'user_no', 'name', 'gender', 'address', 'tel_no', 'status'];

    protected $validationRules = [
        'id_state' => 'required',
        'nric_no' => 'required',
        'name' => 'required',
        'gender' => 'required',
        'address' => 'required',
        'tel_no' => 'required',
    ];
    protected $validationRulesDoctorProfile = [
        'name' => 'required',
        'gender' => 'required',
        'user_no' => 'required',
        'tel_no' => 'required',
    ];

    public function registerPatient($getPost)
    {
        $data = [
            'id_state' => $getPost['id_state'],
            'nric_no' => $getPost['nric_no'],
            'name' => $getPost['name'],
            'gender' => $getPost['gender'],
            'address' => $getPost['address'],
            'tel_no' => $getPost['tel_no'],
        ];

      

        if ($this->save($data))
        {
            return true;
        }
        else
        {
            return $this->errors();
        }
    }

    public function updatePatient($id_personal_detail, $getPost)
    {
        $data = [
            'id' => $id_personal_detail,
            'id_state' => $getPost['id_state'],
            'nric_no' => $getPost['nric_no'],
            'name' => $getPost['name'],
            'gender' => $getPost['gender'],
            'address' => $getPost['address'],
            'tel_no' => $getPost['tel_no'],
        ];

        if ($this->save($data))
        {
            return true;
        }
        else
        {
            return $this->errors();
        }
    }

    public function removePatient($id_personal_detail)
    {
        $data = [
            'id' => $id_personal_detail,
            'status' => -1,
        ];

        if ($this->save($data))
        {
            return true;
        }
        else
        {
            return $this->errors();
        }
    }

    public function getPatient()
    {
        return $this->where('id_user IS NULL', null, false)
            ->where('status', 1)
            ->findAll();
    }

    public function getPatientByID($id_personal_detail)
    {
        return $this->where('id', $id_personal_detail)
            ->first();
    }

    public function findByIDUser($id_user)
    {
        return $this->where('id_user', $id_user)
            ->first();
    }

    public function findAllSubmittedOrder() // For Pharmacist
    {
        return $this->select([
            'DISTINCT(personal_detail.id) AS personal_detail_id',
            'personal_detail.name AS personal_detail_name',
            'personal_detail.nric_no AS personal_detail_nric_no',
        ])
        ->join('pharmacy_medicine', 'pharmacy_medicine.id_patient = personal_detail.id')
        ->where([
            'pharmacy_medicine.medication_status' => 2, // Submitted Order
            'pharmacy_medicine.status' => 1, // Active
        ])
        ->where('personal_detail.id_user IS NULL', null, false)
        ->findAll();
    }

    public function findAllFinishhOrder() // For Receptionist
    {
        return $this->select([
            'DISTINCT(personal_detail.id) AS personal_detail_id',
            'personal_detail.name AS personal_detail_name',
            'personal_detail.nric_no AS personal_detail_nric_no',
        ])
        ->join('pharmacy_medicine', 'pharmacy_medicine.id_patient = personal_detail.id')
        ->where([
            'pharmacy_medicine.medication_status' => 3, // Finish Order - Pending to be Paid
            'pharmacy_medicine.status' => 1, // Active
        ])
        ->where('personal_detail.id_user IS NULL', null, false)
        ->findAll();
    }

    ////////////////////// DOCTOR PROFILE /////////////////////////////

    public function createDoctorProfile($id_user,$getPost)
    {
        $data = [
            'id_user' => $id_user,
            'name' => $getPost['name'],
            'user_no' => $getPost['user_no'],
            'tel_no' => $getPost['tel_no'],
            'gender' => $getPost['gender'],
        ];

        //dd($data);
        
        $this->setValidationRules($this->validationRulesDoctorProfile);

        if ($this->save($data))
        {
            return true;
        }
        else
        {
            return $this->errors();
        }
    }
    public function editDoctorProfile($id, $getPost)
    {   
        $data = [
            'id' => $id,
            'name' => $getPost['name'],
            'user_no' => $getPost['user_no'],
            'tel_no' => $getPost['tel_no'],
            'gender' => $getPost['gender'],
        ];

        //dd($data);
            if ($this->save($data))
                return true;
            else
                return $this->errors();
    }

    public function getDoctorProfileByIDUser($id_user)
    {
        return $this
            ->where([
                'id_user' => $id_user,
                'status' => 1,
            ])
            ->first();
    }
    public function getDoctorProfileByID($id)
    {
        return $this->find($id);
    }
}