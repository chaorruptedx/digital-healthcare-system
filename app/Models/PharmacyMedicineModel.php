<?php namespace App\Models;

use CodeIgniter\Model;

class PharmacyMedicineModel extends Model
{
    protected $table = 'pharmacy_medicine';
    protected $primaryKey = 'id';
    protected $allowedFields = ['id_patient', 'id_pharmacist', 'id_medicine', 'id_bill', 'quantity', 'medication_status', 'status'];

    public function add($id_patient, $getPost)
    {
        $data = [
            'id_patient' => $id_patient,
            'id_medicine' => $getPost['id_medicine'],
            'quantity' => $getPost['quantity'],
            'medication_status' => 1, // Place Order
        ];

        if ($this->save($data))
        {
            return true;
        }
        else
        {
            return $this->errors();
        }
    }

    public function findCurrentOrder($id_patient)
    {
        return $this->select([
            'pharmacy_medicine.id AS pharmacy_medicine_id',
            'pharmacy_medicine.id_patient AS pharmacy_medicine_id_patient',
            'lookup_medicine.name AS lookup_medicine_name',
            'pharmacy_medicine.quantity AS pharmacy_medicine_quantity',
            '(lookup_medicine.price * pharmacy_medicine.quantity) AS pharmacy_medicine_price',
        ])
        ->join('lookup_medicine', 'lookup_medicine.id = pharmacy_medicine.id_medicine')
        ->where([
            'pharmacy_medicine.id_patient' => $id_patient,
            'pharmacy_medicine.medication_status' => 1, // Place Order
            'pharmacy_medicine.status' => 1, // Active
        ])
        ->findAll();
    }

    public function findSubmittedOrder($id_patient)
    {
        return $this->select([
            'pharmacy_medicine.id AS pharmacy_medicine_id',
            'pharmacy_medicine.id_patient AS pharmacy_medicine_id_patient',
            'lookup_medicine.name AS lookup_medicine_name',
            'pharmacy_medicine.quantity AS pharmacy_medicine_quantity',
            '(lookup_medicine.price * pharmacy_medicine.quantity) AS pharmacy_medicine_price',
        ])
        ->join('lookup_medicine', 'lookup_medicine.id = pharmacy_medicine.id_medicine')
        ->where([
            'pharmacy_medicine.id_patient' => $id_patient,
            'pharmacy_medicine.medication_status' => 2, // Submitted Order
            'pharmacy_medicine.status' => 1, // Active
        ])
        ->findAll();
    }

    public function findFinishOrder($id_patient)
    {
        return $this->select([
            'pharmacy_medicine.id AS pharmacy_medicine_id',
            'pharmacy_medicine.id_patient AS pharmacy_medicine_id_patient',
            'lookup_medicine.name AS lookup_medicine_name',
            'pharmacy_medicine.quantity AS pharmacy_medicine_quantity',
            '(lookup_medicine.price * pharmacy_medicine.quantity) AS pharmacy_medicine_price',
        ])
        ->join('lookup_medicine', 'lookup_medicine.id = pharmacy_medicine.id_medicine')
        ->where([
            'pharmacy_medicine.id_patient' => $id_patient,
            'pharmacy_medicine.medication_status' => 3, // Finish Order
            'pharmacy_medicine.status' => 1, // Active
        ])
        ->findAll();
    }

    public function removeOrder($id)
    {
        $data = [
            'id' => $id,
            'status' => -1,
        ];

        if ($this->save($data))
        {
            return true;
        }
        else
        {
            return $this->errors();
        }
    }

    public function submitOrder($id_patient)
    {
        $modelPharmacyMedicines = $this->where([
            'id_patient' => $id_patient,
            'medication_status' => 1, // Place Order
            'status' => 1,
        ])
        ->findAll();
        
        if (!empty($modelPharmacyMedicines) && is_array($modelPharmacyMedicines))
        {
            foreach ($modelPharmacyMedicines as $modelPharmacyMedicine)
            {
                $modelPharmacyMedicineUpdate = new PharmacyMedicineModel();

                $data = [
                    'id' => $modelPharmacyMedicine['id'],
                    'medication_status' => 2, // Submit Order
                ];

                $modelPharmacyMedicineUpdate->save($data);
            }
        }
    }

    public function finishOrder($id_patient) // Pharmacy Already Give Medicines to Patient
    {
        $modelPharmacyMedicines = $this->where([
            'id_patient' => $id_patient,
            'medication_status' => 2, // Submit Order
            'status' => 1,
        ])
        ->findAll();
        
        if (!empty($modelPharmacyMedicines) && is_array($modelPharmacyMedicines))
        {
            foreach ($modelPharmacyMedicines as $modelPharmacyMedicine)
            {
                $modelPharmacyMedicineUpdate = new PharmacyMedicineModel();

                $data = [
                    'id' => $modelPharmacyMedicine['id'],
                    'medication_status' => 3, // Finish Order
                ];

                $modelPharmacyMedicineUpdate->save($data);
            }
        }
    }

    public function payOrder($id_patient) // Pay Medicines
    {
        $modelPharmacyMedicines = $this->where([
            'id_patient' => $id_patient,
            'medication_status' => 3, // Finish Order
            'status' => 1,
        ])
        ->findAll();
        
        if (!empty($modelPharmacyMedicines) && is_array($modelPharmacyMedicines))
        {
            foreach ($modelPharmacyMedicines as $modelPharmacyMedicine)
            {
                $modelPharmacyMedicineUpdate = new PharmacyMedicineModel();

                $data = [
                    'id' => $modelPharmacyMedicine['id'],
                    'medication_status' => 4, // Order Paid
                ];

                $modelPharmacyMedicineUpdate->save($data);
            }
        }
    }
}