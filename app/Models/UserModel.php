<?php namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table = 'user';
    protected $primaryKey = 'id';
    protected $allowedFields = ['email', 'password', 'role', 'status'];

    protected $validationRules = [
        'email' => 'required|valid_email|max_length[255]',
        'password' => 'required',
        'role' => 'required',
    ];

    protected $validationRulesForCreate = [
        'email' => 'required|valid_email|is_unique[user.email]|max_length[255]',
        'password' => 'required',
        'role' => 'required',
        // 'password_confirm' => 'required_with[password]|matches[password]'
    ];

    public function getUser()
    {
        return $this->where('status', 1)
            ->findAll();
    }

    public function getUserByID($id)
    {
        return $this->find($id);
    }

    public function getUserByEmail($email)
    {
        return $this->where([
                'email' => $email,
                'status' => 1
            ])
            ->first();
    }

    public function checkUniqueEmail($email)
    {
        $check_unique_email = $this
            ->where([
                'email' => $email,
                'status' => 1,
            ])
            ->first();

        if (!$check_unique_email)
            return true;
        else
            return false;
    }

    public function authenticateUser($getPost)
    {   
        $authenticateUser = $this->where([
                'email' => $getPost['email'],
                'password' => MD5($getPost['password']),
                'status' => 1
            ])
            ->find();

        if ($authenticateUser)
            return true;
        else
            return false;
    }

    public function registerUser($getPost)
    {   
        $data = [
            'email' => $getPost['email'],
            'password'  => MD5($getPost['password']),
            'role' => $getPost['role'],
        ];

        if (!$this->checkUniqueEmail($getPost['email']))
            $this->setValidationRules($this->validationRulesForCreate);
        
        if ($this->save($data))
            return true;
        else
            return $this->errors();
    }

    public function checkExistingDrProfile($id_user)
    {
        return $this->where([
            'user.id' => $id_user,
        ])
        ->join('personal_detail', 'personal_detail.id_user = user.id')
        ->first();
    }
}