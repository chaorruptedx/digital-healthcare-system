<div class="table-responsive">
    <div class="table-wrapper">
        <table id="data-table" class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center" width="10px">No.</th>
                    <th>Date</th>
                    <th>Weight</th>
                    <th>Height</th>
                    <th>Blood Pressure</th>
                    <th>Doctor Notes</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($modelMedicalHistories) && is_array($modelMedicalHistories)) : ?>
                    <?php $no = 0; ?>
                    <?php foreach ($modelMedicalHistories as $modelMedicalHistory) : ?>
                        <tr>
                            <td class="text-center"><?= ++$no; ?></td>
                            <td><?= $modelMedicalHistory['created_at']; ?></td>
                            <td><?= $modelMedicalHistory['weight']; ?></td>
                            <td><?= $modelMedicalHistory['height']; ?></td>
                            <td><?= $modelMedicalHistory['blood_pressure']; ?></td>
                            <td><?= $modelMedicalHistory['notes']; ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="text-right btn-form form-group">
        <a href="<?= base_url('doctor/Consultation/PatientQueue'); ?>" class="btn btn-secondary"><i class="bi bi-arrow-bar-left"></i>&nbsp;&nbsp;Back</a>
</div>
            
