<?php
use App\Models\BankStatusHelper;
?>

<div class="container ">
  <div class="main-body">    
      <div class="card  bg-light mb-3">
      <h5 class="card-header text-white text-center bg-secondary mb-3">Patient Information Details</h5>
        <div class="card-body ex3">
          <div class="row g-3">
              <div class="col-md-6">
                <label for="name" class="form-label fw-bold ">Patient Name:</label>
                <?= $modelPatientQueue['patient_detail_name']; ?>
              </div>
            <div class="col-md-6">
                <label for="nric_no" class="form-label fw-bold ">NRIC No:</label>
                <?= $modelPatientQueue['patient_detail_nric_no']; ?>
            </div>
            <div class="col-md-6">
              <label for="tel_no" class="form-label fw-bold ">Phone Number:</label>
              <?= $modelPatientQueue['patient_detail_tel_no']; ?>
            </div>
            <div class="col-md-6">
              <label for="gender" class="form-label fw-bold ">Gender:</label>
              <?= gender_name($modelPatientQueue['patient_detail_gender']); ?>
            </div>
            <div class="col-md-6">
              <label for="address" class="form-label fw-bold ">Patient Address:</label>
              <?= $modelPatientQueue['patient_detail_address']; ?>
            </div>
            <div class="col-md-6">
              <label for="inputState" class="form-label  fw-bold">State:</label>
              <?= BankStatusHelper::getStateName($modelPatientQueue['patient_detail_id_state']); ?>
            </div>
          </div>
        </div>
      </div>
      <br>

      <form action="<?= base_url('doctor/Consultation/SavePatientConsultation/'.$modelPatientQueue['patient_queue_id']); ?>" method="post" >
    <?= csrf_field() ?>
        <div class="card  bg-light mb-3">
            <h5 class="card-header text-white text-center bg-primary mb-3">Consultation</h5>
            <div class="card-body">
                <div class="row g-3">
                    <div class="col-md-4">
                        <label for="weight" class="form-label fw-bold ">Weight</label>
                        <input type="number" name="weight" value="<?= isset($modelMedicalHistory['weight']) ? $modelMedicalHistory['weight'] : null ?>" placeholder="Enter patient Weight" class="form-control" required/>
                    </div>
                    <div class="col-md-4">
                        <label for="nric_no" class="form-label fw-bold ">Height</label>
                        <input type="number" name="height" value="<?= isset($modelMedicalHistory['height']) ? $modelMedicalHistory['height'] : null ?>" placeholder="Enter patient Height" class="form-control" required/>
                    </div>
                    <div class="col-md-4">
                        <label for="blood_pressure" class="form-label fw-bold ">Blood Pressure</label>
                        <input type="number" name="blood_pressure" value="<?= isset($modelMedicalHistory['blood_pressure']) ? $modelMedicalHistory['blood_pressure'] : null ?>" placeholder="Enter Patient BP" class="form-control" required/>
                    </div>
                    <div class="col-md-12">
                        <label for="notes" class="form-label fw-bold ">Notes</label>
                        <textarea type="text" name="notes" placeholder="Write some notes" class="form-control"><?= isset($modelMedicalHistory['notes']) ? $modelMedicalHistory['notes'] : null ?></textarea>
                    </div>
                    <div class="text-right btn-form form-group">
                        <button type="submit" name="submit" class="btn btn-success"><i class="bi bi-check-lg"></i>&nbsp;&nbsp;Submit</button>
                    </div>
                </div>
            </div>   
        <div>
    </form>
            
    <h5 class="card-header text-white text-center bg-primary mb-3">Order Medicine</h5>
        <div class="card-body">
            <a href="<?= base_url('doctor/Medicine/make_order/'.$modelPatientQueue['patient_queue_id']);?>" class="btn btn-success"><i class="bi bi-cart-plus-fill"></i>&nbsp;&nbsp;Add Order</a><br><br>
            <div class="table-responsive">
                <div class="table-wrapper">
                    <table id="data-table" class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center" width="10px">No.</th>
                            <th>Name</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($modelPharmacyMedicines) && is_array($modelPharmacyMedicines)) : ?>
                            <?php $no = 0; ?>
                            <?php foreach ($modelPharmacyMedicines as $modelPharmacyMedicine) : ?>
                                <tr>
                                    <td class="text-center"><?= ++$no; ?></td>
                                    <td><?= $modelPharmacyMedicine['lookup_medicine_name']; ?></td>
                                    <td><?= $modelPharmacyMedicine['pharmacy_medicine_quantity']; ?></td>
                                    <td><?= $modelPharmacyMedicine['pharmacy_medicine_price']; ?></td>
                                    </td>
                                    <td class="text-center">
                                        <a href="#"><i class="bi bi-trash-fill" data-bs-toggle="modal" data-bs-target="#myModal<?= $modelPharmacyMedicine['pharmacy_medicine_id']; ?>"></i></a>
                                    </td> 
                                </tr>
                                <!-- The Modal -->
                                <div class="modal fade" id="myModal<?= $modelPharmacyMedicine['pharmacy_medicine_id']; ?>" tabindex="-1">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <!-- Modal Header -->
                                            <div class="modal-header bg-danger">
                                                <h4 class="modal-title text-white">Remove Medicine Order</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <!-- Modal body -->
                                            <div class="modal-body">
                                                Are you sure you want to remove medicine order <b><?= esc($modelPharmacyMedicine['lookup_medicine_name']); ?></b>?
                                            </div>
                                            <!-- Modal footer -->
                                            <div class="modal-footer">
                                                <a href="<?= base_url('doctor/Medicine/remove_order/'.$modelPharmacyMedicine['pharmacy_medicine_id'].'/'.$modelPatientQueue['patient_queue_id']); ?>" type="button" class="btn btn-success"><span class="bi bi-check-lg"></span>&nbsp;&nbsp;Yes</a>
                                                <button type="button" class="btn btn-danger" data-bs-dismiss="modal"><span class="bi bi-x-lg"></span>&nbsp;&nbsp;No</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                    </table>
                </div>
                <form action="<?= base_url('doctor/medicine/submit-order/'.$modelPatientQueue['personal_detail_id'].'/'.$modelPatientQueue['patient_queue_id']); ?>" method="post" >
                    <?= csrf_field() ?>
                    <div class="text-right btn-form form-group">
                        <a href="<?= base_url('doctor/Consultation/PatientQueue'); ?>" class="btn btn-secondary"><i class="bi bi-arrow-bar-left"></i>&nbsp;&nbsp;Back</a>
                        <button type="submit" name="submit" class="btn btn-success"><i class="bi bi-check-lg"></i>&nbsp;&nbsp;Submit</button>
                    </div>
                </form></br>
            </div>
        </div>
    <div>
 </div>

    
            
            
                    