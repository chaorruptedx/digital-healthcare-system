<div class="table-responsive">
    <div class="table-wrapper">
            <table id="data-table" class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center" width="10px">No.</th>
                        <th>Name</th>
                        <th>NRIC</th>
                        <th class="text-center" width="200px">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($modelPatientQueues) && is_array($modelPatientQueues)) : ?>
                        <?php foreach ($modelPatientQueues as $mpq_key => $modelPatientQueue) : ?>
                            <tr>
                                <td class="text-center"><?= ++$mpq_key ?></td>
                                <td><?= $modelPatientQueue['patient_detail_name'] ?></td>
                                <td><?= $modelPatientQueue['patient_detail_nric_no'] ?></td>
                                <td>
                                    <a href="<?= base_url('doctor/Consultation/PatientConsultation/'.$modelPatientQueue['patient_queue_id']); ?>" type="button" class="btn btn-outline-info">Consult</a>
                                    <a href="<?= base_url('doctor/Consultation/MedicalHistory/'.$modelPatientQueue['patient_queue_id']); ?>" type="button" class="btn btn-outline-warning">Medical History</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif ?>
                </tbody>
            </table>
    </div>
</div>
            
