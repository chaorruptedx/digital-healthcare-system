<div class="container">
  <div class="main-body">    
    <form action="" method="post" >
      <?= csrf_field() ?>
      <div class="card  bg-light mb-3">
      <h5 class="card-header text-center bg-warning mb-3">Edit your order</h5>
        <div class="card-body">
          <div class="row g-3">
              <div class="col-md-6">
                <label for="medicine" class="form-label fw-bold ">Medicine Name</label>
                <select class="form-select" aria-label="Default select example">
                  <option selected>Select Medicine</option>
                  <option value="1">One</option>
                  <option value="2">Two</option>
                  <option value="3">Three</option>
                </select>
              </div>
              <div class="col-md-6">
                <label for="medicine" class="form-label fw-bold ">Quantity</label>
                <select class="form-select" aria-label="Default select example">
                  <option selected>Select Quantity</option>
                  <option value="1">One</option>
                  <option value="2">Two</option>
                  <option value="3">Three</option>
                </select>
              </div>
          </div>
        </div>
      </div>
      <div class="text-right btn-form form-group">
        <a href="<?= base_url('doctor/Consultation/PatientConsultation'); ?>" class="btn btn-secondary"><i class="bi bi-arrow-bar-left"></i>&nbsp;&nbsp;Back</a>
        <button type="submit" name="submit" class="btn btn-success"><i class="bi bi-check-lg"></i>&nbsp;&nbsp;Submit Order</button>
      </div>
    <form>
  </div>
</div>