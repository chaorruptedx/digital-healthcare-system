<div class="container">
  <div class="main-body">    
    <form action="<?= base_url('doctor/Medicine/make_order/'.$id_patient_queue);?>" method="post" >
      <?= csrf_field() ?>
      <div class="card  bg-light mb-3">
      <h5 class="card-header text-center bg-warning mb-3">Make your order</h5>
        <div class="card-body">
          <div class="row g-3">
              <div class="col-md-6">
                <label for="medicine" class="form-label fw-bold ">Medicine Name</label>
                <select name="id_medicine" class="form-select" aria-label="Default select example" required>
                  <option selected>Select Medicine ...</option>
                  <?php if (!empty($modelLookupMedicines) && is_array($modelLookupMedicines)) : ?>
                    <?php foreach ($modelLookupMedicines as $modelLookupMedicine) : ?>
                      <option value="<?= $modelLookupMedicine['id'] ?>"><?= $modelLookupMedicine['name'] ?></option>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </select>
              </div>
              <div class="col-md-6">
                <label for="quantity" class="form-label fw-bold ">Quantity</label>
                <input type="quantity" name="quantity" placeholder="Enter Quantity" class="form-control" required/>
              </div>
          </div>
        </div>
      </div>
      <div class="text-right btn-form form-group">
        <a href="<?= base_url('doctor/Consultation/PatientConsultation/'.$id_patient_queue); ?>" class="btn btn-secondary"><i class="bi bi-arrow-bar-left"></i>&nbsp;&nbsp;Back</a>
        <button type="submit" name="submit" class="btn btn-success"><i class="bi bi-check-lg"></i>&nbsp;&nbsp;Submit Order</button>
      </div>
    <form>
  </div>
</div>