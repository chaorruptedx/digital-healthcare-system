<div class="container">
  <div class="main-body">    
    <form action="<?= base_url('doctor/Profile/create_profile'); ?>" method="post" >
    <?= csrf_field() ?>
      <div class="card  bg-light mb-3">
        <h5 class="card-header text-white text-center bg-primary mb-3">Doctor Information</h5>
          <div class="card-body">
            <div class="row g-3">
              <div class="col-md-6">
                <label for="name" class="form-label fw-bold ">Doctor Name</label>
                <input type="name" name="name" placeholder="Enter your name" class="form-control"/>
              </div>
            <div class="col-md-6">
                <label for="user_no" class="form-label fw-bold ">Staff ID.</label>
                <input type="user_no" name="user_no" placeholder="Enter your Staff ID" class="form-control"/>
            </div>
            <div class="col-md-6">
              <label for="tel_no" class="form-label fw-bold ">Phone Number</label>
              <input type="number" name="tel_no" placeholder="Enter Phone Number" class="form-control"/>
            </div>
            <div class="col-md-6">
              <label for="gender" class="form-label fw-bold ">Gender</label>
              <div class="form-check">
              <input type="radio" id="male" name="gender" value="1">
                <label class="form-check-label" for="gender">Male</label>
              </div>
              <div class="form-check">
              <input type="radio" id="female" name="gender" value="2">
                <label class="form-check-label" for="gender">Female</label>
              </div>
            </div>
          </div>
      </div>
    </div>
      <div class="text-right btn-form form-group">
        <a href="<?= base_url('doctor/home'); ?>" class="btn btn-secondary"><i class="bi bi-arrow-bar-left"></i>&nbsp;&nbsp;Back</a>
        <button type="submit" name="submit" class="btn btn-success"><i class="bi bi-check-lg"></i>&nbsp;&nbsp;Create Profile</button>
      </div>
  <form>
  </div>
</div>