<div class="container">
  <div class="main-body">    
      <div class="card  bg-light mb-3">
      <h5 class="card-header text-white text-center bg-primary mb-3">Doctor Information</h5>
        <div class="card-body">
          <div class="row g-3">
              <div class="col-md-6">
                <label for="name" class="form-label fw-bold ">Doctor Name:   <?= $doctor_profile['name']; ?></label>
               
              </div>
            <div class="col-md-6">
                <label for="user_no" class="form-label fw-bold ">Staff ID:   <?= $doctor_profile['user_no']; ?></label>
                
            </div>
            <div class="col-md-6">
              <label for="tel_no" class="form-label fw-bold ">Phone Number:   <?= $doctor_profile['tel_no']; ?></label>
              
            </div>
            <div class="col-md-6">
              <label for="gender" class="form-label fw-bold ">Gender:   
                <?php
                  if (esc($doctor_profile['gender']) == '1') :
                  echo 'Male';
                  elseif (esc($doctor_profile['gender']) == '2') :
                  echo 'Female';
                  endif;
                ?>
              </label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="text-right btn-form form-group">
        <a href="<?= base_url('doctor/home'); ?>" class="btn btn-secondary"><i class="bi bi-arrow-bar-left"></i>&nbsp;&nbsp;Back</a>
        <a href="<?= base_url('doctor/Profile/edit_profile/'.$doctor_profile['id']); ?>"class="btn btn-primary"><i class="bi bi-pencil-fill"></i>&nbsp;&nbsp;Updated Profile</a>
      </div>
  </div>
</div>