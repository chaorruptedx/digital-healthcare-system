<div class="container">
    <div class="main-body"> 
        <form action="<?= base_url('pharmacist/Medicine/edit_medicine/'.$medicine['id']); ?>" method="post" >
            <?= csrf_field() ?>
            <div class="card  bg-light mb-3">
                <h5 class="card-header text-white text-center bg-primary mb-3">Medicine Information</h5>
                <div class="card-body">
                    <div class="mb-3">
                        <label for="code" class="form-label"><strong>Code</strong></label>
                        <input type="code" name="code" class="form-control" id="code" value="<?= $medicine['code']; ?>" required>
                        <div class="invalid-feedback">
                            Please enter a medicine code.
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="name" class="form-label"><strong>Medicine Name</strong></label>
                        <input type="name" name="name" class="form-control" id="name" value="<?= $medicine['name']; ?>" required>
                        <div class="invalid-feedback">
                            Please enter medicine Name
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="price" class="form-label"><strong>Price</strong></label>
                        <input type="price" name="price" class="form-control" id="price" value="<?= $medicine['price']; ?>" required>
                        <div class="invalid-feedback">
                            Please enter medicine Price
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-right btn-form form-group">
             <a href="<?= base_url('pharmacist/Medicine/list_of_medicine'); ?>" class="btn btn-secondary"><i class="bi bi-arrow-bar-left"></i>&nbsp;&nbsp;Back</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
    </div>
</div>