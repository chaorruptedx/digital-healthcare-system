<div class="table-responsive">
    <div class="table-wrapper">
        <a href="<?= base_url('pharmacist/Medicine/register_medicine'); ?>"  class="btn btn-success"><i class="bi bi-file-plus"></i>&nbsp;&nbsp;Add Medicine</a><br><br>   
            <table id="data-table" class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center" width="10px">No.</th>
                        <th>Code</th>
                        <th>Medicine Name</th>
                        <th>Price</th>
                        <th class="text-center" width="110px">Actions</th>
                    </tr>
                </thead>
                <tbody>
                <?php $no=0; ?>
                <?php if (!empty($medicine) && is_array($medicine)) : ?>
                    <?php foreach ($medicine as $medicine_data) : ?>
                    <tr>
                        <td class="text-center"><?= ++$no ?></td>
                        <td><?= esc($medicine_data['code']); ?></td>
                        <td><?= esc($medicine_data['name']); ?></td>
                        <td><?= esc($medicine_data['price']); ?></td>
                        <td class="text-center">
                        <!--a href="<?= base_url('receptionist/Patient_Registration/ViewPatientDetails'); ?>"><i class="bi bi-eye-fill"></i></a>  < view medicine details -->
                        &nbsp;
                        <a href="<?= base_url('pharmacist/Medicine/edit_medicine/'.$medicine_data['id']); ?>"><i class="bi bi-pencil-fill"></i></a>  <!-- edit medicine details -->
                        &nbsp;
                        <a href="#" data-bs-toggle="modal" data-bs-target="#myModal<?= $medicine_data['id']; ?>"><i class="bi bi-trash-fill"></i></a> <!-- delete medicine -->
                        </td>
                        <!-- The Modal -->
                        <div class="modal fade" id="myModal<?= $medicine_data['id']; ?>" tabindex="-1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header bg-danger">
                                            <h4 class="modal-title text-white">Remove Medicine</h4>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            Are you sure you want to remove medicine <b><?= esc($medicine_data['name']); ?></b>?
                                        </div>
                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <a href="<?= base_url('pharmacist/Medicine/remove_medicine/'.$medicine_data['id']);?>" type="button" class="btn btn-success"><span class="bi bi-check-lg"></span>&nbsp;&nbsp;Yes</a>
                                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal"><span class="bi bi-x-lg"></span>&nbsp;&nbsp;No</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </tr>
                    <?php endforeach; ?>
                <?php endif ?>
                </tbody>
            </table>
    </div>
</div>