<form action="<?= base_url('pharmacist/Medicine/register_medicine'); ?>" method="post" >
<?= csrf_field() ?>
    <div class="mb-3">
        <label for="code" class="form-label">Code</label>
        <input type="code" name="code" class="form-control" id="code" required>
        <div class="invalid-feedback">
            Please enter a medicine code.
        </div>
    </div>
    <div class="mb-3">
        <label for="name" class="form-label">Medicine Name</label>
        <input type="name" name="name" class="form-control" id="name" required>
        <div class="invalid-feedback">
            Please enter medicine Name
        </div>
    </div>
    <div class="mb-3">
        <label for="price" class="form-label">Price</label>
        <input type="price" name="price" class="form-control" id="price" required>
        <div class="invalid-feedback">
            Please enter medicine Price
        </div>
    </div>
    
    <a href="<?= base_url('pharmacist/Medicine/list_of_medicine'); ?>" class="btn btn-secondary"><i class="bi bi-arrow-bar-left"></i>&nbsp;&nbsp;Back</a>
    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>