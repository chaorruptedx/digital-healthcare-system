<div class="table-responsive">
    <div class="table-wrapper">
            <table id="data-table" class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center" width="10px">No.</th>
                        <th>Patient Name</th>
                        <th>NRIC No.</th>
                        <th class="text-center" width="110px">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($modelPharmacyMedicines) && is_array($modelPharmacyMedicines)) : ?>
                        <?php $no = 0; ?>
                        <?php foreach ($modelPharmacyMedicines as $modelPharmacyMedicine) : ?>
                            <tr>
                                <td class="text-center"><?= ++$no; ?></td>
                                <td><?= $modelPharmacyMedicine['personal_detail_name'] ?></td>
                                <td><?= $modelPharmacyMedicine['personal_detail_nric_no'] ?></td>
                                <td class="text-center">
                                <a href="<?= base_url('pharmacist/Order/view_order/'.$modelPharmacyMedicine['personal_detail_id']); ?>"type="button" class="btn btn-info">Order Details</a>
                                    &nbsp;
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
    </div>
</div>