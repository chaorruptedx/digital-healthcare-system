<div class="container">
  <div class="main-body">    
    <div class="card  bg-light mb-3">
        <h5 class="card-header text-white text-center bg-primary mb-3">Order Information</h5>
            <div class="card-body">
                <div class="row g-3">
                    <!-- <div class="col-md-6">
                        <label for="name" class="form-label fw-bold ">Doctor Name:</label>
                    </div>
                <div class="col-md-6">
                    <label for="user_no" class="form-label fw-bold ">Staff ID:</label>
                </div> -->
                <div class="col-md-6">
                    <label for="name" class="form-label fw-bold ">Patient Name:</label>
                    <?= $modelPersonalDetailPatient['name']; ?>
                </div>
                <div class="col-md-6">
                    <label for="nric_no" class="form-label fw-bold ">NRIC No:</label> 
                    <?= $modelPersonalDetailPatient['nric_no']; ?>
                </div>
                <div class="col-md-6">
                    <label for="tel_no" class="form-label fw-bold ">Phone Number:</label>
                    <?= $modelPersonalDetailPatient['tel_no']; ?>
                </div>
                <div class="col-md-6">
                    <label for="gender" class="form-label fw-bold ">Gender:</label>
                    <?= gender_name($modelPersonalDetailPatient['gender']); ?>
                </div>
                <h4> List of Order Medicine </h4>
                <div class="table-responsive">
                    <div class="table-wrapper">
                        <table id="data-table" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center" width="10px">No.</th>
                                    <th>Name</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <!-- <th>Actions</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($modelPharmacyMedicines) && is_array($modelPharmacyMedicines)) : ?>
                                    <?php foreach ($modelPharmacyMedicines as $modelPharmacyMedicine) : ?>
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td><?= $modelPharmacyMedicine['lookup_medicine_name']; ?></td>
                                            <td><?= $modelPharmacyMedicine['pharmacy_medicine_quantity']; ?></td>
                                            <td><?= $modelPharmacyMedicine['pharmacy_medicine_price']; ?></td>
                                            </td>
                                        <!-- <td class="text-center">
                                            <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
                                            <label class="form-check-label" for="flexCheckChecked">Ready</label>
                                        </td>  -->
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
      <div class="text-right btn-form form-group">
        <a href="<?= base_url('pharmacist/Order/list_of_order'); ?>" class="btn btn-secondary"><i class="bi bi-arrow-bar-left"></i>&nbsp;&nbsp;Back</a>
        <a href="<?= base_url('pharmacist/Order/accept_order/'.$modelPersonalDetailPatient['id']); ?>"class="btn btn-primary"><i class="bi bi-check-lg"></i>&nbsp;&nbsp;Submit</a>
      </div>
  </div>
</div>