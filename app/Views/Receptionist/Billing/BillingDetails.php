<?php
use App\Models\BankStatusHelper;
?>
<div class="container">
  <div class="main-body">    
        <div class="card  bg-light">
        <h5 class="card-header text-white text-center bg-primary mb-3">Medical Invoice</h5>    
            <div class="card-body ex3">
                <div class="row g-3">
                    <div class="col-md-6">
                        <label for="name" class="form-label fw-bold ">Patient Name:</label>
                        <?= $modelPersonalDetailPatient['name']; ?>
                    </div>
                    <div class="col-md-6">
                        <label for="nric_no" class="form-label fw-bold ">NRIC No:</label>   
                        <?= $modelPersonalDetailPatient['nric_no']; ?>
                    </div>
                    <div class="col-md-6">
                        <label for="tel_no" class="form-label fw-bold ">Phone Number:</label>
                        <?= $modelPersonalDetailPatient['tel_no']; ?>
                    </div>
                    <div class="col-md-6">
                        <label for="gender" class="form-label fw-bold ">Gender:</label>
                        <?= gender_name($modelPersonalDetailPatient['gender']); ?>
                    </div>
                    <div class="col-md-6">
                        <label for="address" class="form-label fw-bold ">Patient Address:</label>
                        <?= $modelPersonalDetailPatient['address']; ?>
                    </div>
                    <div class="col-md-6">
                        <label for="inputState" class="form-label  fw-bold">State:</label>
                        <?= BankStatusHelper::getStateName($modelPersonalDetailPatient['id_state']); ?>
                    </div>
                </div>
                <div class="border-top my-3"></div>
                    <div class="table-responsive">
                        <table class="table align-middle table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">Medical Services and Medication</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Price</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($modelPharmacyMedicines) & is_array($modelPharmacyMedicines)) : ?>
                                    <?php $no = 0; $total_price = 0; ?>
                                    <?php foreach ($modelPharmacyMedicines as $modelPharmacyMedicine) : ?>
                                        <tr>
                                            <th scope="row"><?= ++$no; ?></th>
                                            <td><?= $modelPharmacyMedicine['lookup_medicine_name']; ?></td>
                                            <td><?= $modelPharmacyMedicine['pharmacy_medicine_quantity']; ?></td>
                                            <td><?= $modelPharmacyMedicine['pharmacy_medicine_price']; ?></td>
                                            <?php $total_price += $modelPharmacyMedicine['pharmacy_medicine_price']; ?>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row g-3">
                        <div class="col-md-8"></div>
                            <div class="col-md-4">
                                <label for="nric_no" class="form-label fw-bold ">Total Price:</label>   
                                <?= number_format((float) $total_price, 2, '.', ''); ?>
                            </div>
                        </div>
                        <div class="row g-3">
                        <div class="col-md-8"></div>
                            <div class="col-md-4">
                                <label for="nric_no" class="form-label fw-bold ">Payment Status: UNPAID</label>   
                            </div>
                        </div>
                        <div class="text-right btn-form form-group">
                        <input id="url-pay" type="hidden" value="<?= base_url('receptionist/Billing/Paid/'.$modelPersonalDetailPatient['id']); ?>">
                        <button id="pay-bill" class="btn btn-success "><i class="bi bi-cash-coin"></i>&nbsp;&nbsp;Pay Now</button>
                    </div>
                </div>
            </div>    
        </div>
    </div>
</div>