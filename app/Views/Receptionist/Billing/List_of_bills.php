<div class="table-responsive">
    <div class="table-wrapper">
        <table id="data-table" class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center" width="10px">No.</th>
                    <th>Patient Name</th>
                    <th>NRIC</th>
                    <th class="text-center" width="110px">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($modelPersonalDetailPatients) && is_array($modelPersonalDetailPatients)) : ?>
                    <?php foreach ($modelPersonalDetailPatients as $modelPersonalDetailPatient) : ?>
                        <tr>
                            <td class="text-center"></td>
                            <td><?= $modelPersonalDetailPatient['personal_detail_name'] ?></td>
                            <td><?= $modelPersonalDetailPatient['personal_detail_nric_no'] ?></td>
                            <td class="text-center">
                                <a href="<?= base_url('receptionist/Billing/BillingDetails/'.$modelPersonalDetailPatient['personal_detail_id']); ?>"><i class="bi bi-eye-fill"></i></a>
                                &nbsp;
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>

