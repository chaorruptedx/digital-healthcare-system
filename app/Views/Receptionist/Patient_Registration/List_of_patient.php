<div class="table-responsive">
    <div class="table-wrapper">
        <a href="<?= base_url('receptionist/Patient_Registration/RegisterPatient'); ?>"  class="btn btn-success"><i class="bi bi-person-plus"></i>&nbsp;&nbsp;Register Patient</a><br><br>   
            <table id="data-table" class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center" width="10px">No.</th>
                        <th>Name</th>
                        <th>NRIC</th>
                        <th class="text-center" width="110px">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($listPatient) && is_array($listPatient)) : ?>
                        <?php foreach ($listPatient as $key => $value) : ?>
                            <tr>
                                <td class="text-center"><?= ++$key ?></td>
                                <td><?= $value['name'] ?></td>
                                <td><?= $value['nric_no'] ?></td>
                                <td class="text-center">
                                    <a href="<?= base_url('receptionist/Patient_Registration/ViewPatientDetails/'.$value['id']); ?>"><i class="bi bi-eye-fill"></i></a>  <!-- view patient details -->
                                    &nbsp;
                                    <a href="<?= base_url('receptionist/Patient_Registration/EditPatient/'.$value['id']); ?>"><i class="bi bi-pencil-fill"></i></a>  <!-- edit patient details -->
                                    &nbsp;
                                    <a href="#" data-bs-toggle="modal" data-bs-target="#myModal<?= $value['id']; ?>"><i class="bi bi-trash-fill"></i></a> <!-- delete patient -->
                                    &nbsp;
                                    <a href="#" data-bs-toggle="modal" data-bs-target="#queueModal<?= $value['id']; ?>"><i class="bi bi-person-plus-fill"></i></a>
                                </td>
                            </tr>
                            <!-- The Modal -->
                            <div class="modal fade" id="myModal<?= $value['id']; ?>" tabindex="-1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header bg-danger">
                                            <h4 class="modal-title text-white">Remove Patient</h4>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            Are you sure you want to remove patient <b><?= esc($value['name']); ?></b>?
                                        </div>
                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <a href="<?= base_url('receptionist/Patient_Registration/RemovePatient/'.$value['id']);?>" type="button" class="btn btn-success"><span class="bi bi-check-lg"></span>&nbsp;&nbsp;Yes</a>
                                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal"><span class="bi bi-x-lg"></span>&nbsp;&nbsp;No</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- The Modal for Patient Queue -->
                            <div class="modal fade" id="queueModal<?= $value['id']; ?>" tabindex="-1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header bg-primary">
                                            <h4 class="modal-title text-white">Patient Queue</h4>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            Are you sure you want to add patient <b><?= esc($value['name']); ?></b> to the queue list?
                                        </div>
                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <a href="<?= base_url('receptionist/Patient_Registration/AddPatientQueue/'.$value['id']);?>" type="button" class="btn btn-success"><span class="bi bi-check-lg"></span>&nbsp;&nbsp;Yes</a>
                                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal"><span class="bi bi-x-lg"></span>&nbsp;&nbsp;No</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif ?>
                </tbody>
            </table>
    </div>
</div> 
