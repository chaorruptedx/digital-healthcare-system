<div class="container">
  <div class="main-body">    
    <form action="<?= base_url('receptionist/Patient_Registration/RegisterPatient'); ?>" method="post" >
      <?= csrf_field() ?>
      <div class="card  bg-light mb-3">
      <h5 class="card-header text-white bg-primary mb-3">Patient Information</h5>
        <div class="card-body">
          <div class="row g-3">
              <div class="col-md-6">
                <label for="name" class="form-label fw-bold ">Patient Name</label>
                <input type="name" name="name" placeholder="Enter patient name" class="form-control" required/>
              </div>
            <div class="col-md-6">
                <label for="nric_no" class="form-label fw-bold ">NRIC NO.</label>
                <input type="number" name="nric_no" placeholder="Enter patient NRIC" class="form-control" required/>
            </div>
            <div class="col-md-6">
              <label for="tel_no" class="form-label fw-bold ">Phone Number</label>
              <input type="number" name="tel_no" placeholder="Enter Phone Number" class="form-control" required/>
            </div>
            <div class="col-md-6">
              <label for="gender" class="form-label fw-bold ">Gender</label>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="gender" value="1" required>
                <label class="form-check-label" for="gender">Male</label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="gender" value="2" required>
                <label class="form-check-label" for="gender">Female</label>
              </div>
            </div>
            <div class="col-md-6">
              <label for="address" class="form-label fw-bold ">Patient Address</label>
              <textarea type="address" name="address" placeholder="Enter patient Address" class="form-control" required></textarea>
            </div>
            <div class="col-md-6">
              <label for="inputState" class="form-label">State</label>
              <select id="inputState" name="id_state" class="form-select" required>
                <option>Choose...</option>
                <?php if (!empty($listState) && is_array($listState)) : ?>
                  <?php foreach ($listState as $value) : ?>
                    <option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                  <?php endforeach; ?>
                <?php endif; ?>
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="text-right btn-form form-group">
        <a href="<?= base_url('receptionist/Patient_Registration/List_of_patient'); ?>" class="btn btn-secondary"><i class="bi bi-arrow-bar-left"></i>&nbsp;&nbsp;Back</a>
        <button type="submit" name="submit" class="btn btn-success"><i class="bi bi-check-lg"></i>&nbsp;&nbsp;Register Patient</button>
      </div>
    </form>
  </div>
</div>