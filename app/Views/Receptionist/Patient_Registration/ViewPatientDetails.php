<?php
use App\Models\BankStatusHelper;
?>

<div class="container">
  <div class="main-body">    
      <div class="card  bg-light mb-3">
      <h5 class="card-header text-white bg-primary mb-3">Patient Information Details</h5>
        <div class="card-body">
          <div class="row g-3">
              <div class="col-md-6">
                <label for="name" class="form-label fw-bold ">Patient Name:</label>
                <?= $patient_personal_detail_data['name'] ?>
              </div>
            <div class="col-md-6">
                <label for="nric_no" class="form-label fw-bold ">NRIC No:</label>
                <?= $patient_personal_detail_data['nric_no'] ?>
            </div>
            <div class="col-md-6">
              <label for="tel_no" class="form-label fw-bold ">Phone Number:</label>
              <?= $patient_personal_detail_data['tel_no'] ?>
            </div>
            <div class="col-md-6">
              <label for="gender" class="form-label fw-bold ">Gender:</label>
              <?= gender_name($patient_personal_detail_data['gender']) ?>
            </div>
            <div class="col-md-6">
              <label for="address" class="form-label fw-bold ">Patient Address:</label>
              <?= $patient_personal_detail_data['address'] ?>
            </div>
            <div class="col-md-6">
              <label for="inputState" class="form-label  fw-bold">State:</label>
              <?= BankStatusHelper::getStateName($patient_personal_detail_data['id_state']) ?>
            </div>
          </div>
        </div>
      </div>
      <div class="text-right btn-form form-group">
        <a href="<?= base_url('receptionist/Patient_Registration/List_of_patient'); ?>" class="btn btn-secondary"><i class="bi bi-arrow-bar-left"></i>&nbsp;&nbsp;Back</a>
        <a href="<?= base_url('receptionist/Patient_Registration/EditPatient/'.$patient_personal_detail_data['id']); ?>"class="btn btn-primary"><i class="bi bi-pencil-fill"></i>&nbsp;&nbsp;Update Patient</a>
      </div>
  </div>
</div>