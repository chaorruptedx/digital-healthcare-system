<table id="data-table">
    <thead>
        <tr>
            <th width="10px">No.</th>
            <th>Name</th>
            <th>NRIC</th>
            <th>Time Start Queue</th>
        </tr>
    </thead>
    <tbody>
    <?php if (!empty($modelPatientQueues) && is_array($modelPatientQueues)) : ?>
        <?php foreach ($modelPatientQueues as $mpq_key => $modelPatientQueue) : ?>
            <tr>
                <td><?= ++$mpq_key ?></td>
                <td><?= $modelPatientQueue['patient_detail_name'] ?></td>
                <td><?= $modelPatientQueue['patient_detail_nric_no'] ?></td>
                <td><?= $modelPatientQueue['patient_detail_created_at'] ?></td>
            </tr>
        <?php endforeach; ?>
    <?php endif ?>
    </tbody>
</table>
