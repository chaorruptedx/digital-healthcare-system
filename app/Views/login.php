<main class="form-signin">
    <form action="<?= base_url('/login'); ?>" method="post" class="needs-validation" novalidate>
        <?= csrf_field(); ?>
        <h1 class="h3 mb-3 fw-normal">Please sign in</h1>

        <div class="form-floating">
            <input type="email" name="email" class="form-control" id="floatingInput" placeholder="name@example.com" required>
            <label for="floatingInput">Email address</label>
            <div class="invalid-feedback">
                Please enter a valid email address.
            </div>
        </div>
        <div class="form-floating">
            <input type="password" name="password" class="form-control" id="floatingPassword" placeholder="Password" required>
            <label for="floatingPassword">Password</label>
            <div class="invalid-feedback">
                Please enter your password.
            </div>
        </div>

        <button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>
    </form>
</main>