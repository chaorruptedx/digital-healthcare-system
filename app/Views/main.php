<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Digital Healthcare Information System</title>
	<meta name="description" content="A system designed to manage healthcare data">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" type="image/png" href="<?= base_url('digital-healthcare.ico'); ?>"/>

	<!-- STYLES -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('css/bootstrap/bootstrap.min.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('css/bootstrap/icons/bootstrap-icons.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('css/login/login.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('css/select2/select2.min.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('css/data-tables/dataTables.bootstrap5.min.css'); ?>">
</head>
<header class="p-3 bg-dark text-white">
    <div class="container">
      	<div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
			<a href="<?= base_url('/'); ?>" class="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
				<img src="<?= base_url('digital-healthcare.ico'); ?>" alt="Digital Healthcare Information System">
			</a>

        <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
			<?php if (session()->has('id') && session()->get('role') == 1) : ?> <!-- Receptionist -->
				<li><a href="<?= base_url('receptionist/home'); ?>" class="nav-link px-2 <?= active_link(['HomeController'], ['index']); ?>">Home</a></li>
				<li><a href="<?= base_url('receptionist/Patient_Registration/List_of_patient'); ?>" class="nav-link px-2 <?= active_link(['PatientRegistrationController'], ['index', 'register_patient', 'view_patient', 'edit_patient']); ?>">Patient Management</a></li>
				<li><a href="<?= base_url('receptionist/patient-queue/queue-list'); ?>" class="nav-link px-2 <?= active_link(['PatientQueueController'], ['index']); ?>">Patient Queue List</a></li>
				<li><a href="<?= base_url('receptionist/Billing/List_of_bills'); ?>" class="nav-link px-2 <?= active_link(['BillingController'], ['index', 'view_payment_details']); ?>">Bills Management</a></li>

			<?php elseif (session()->has('id') && session()->get('role') == 2) : ?> <!-- Doctor -->
				<li><a href="<?= base_url('doctor/home'); ?>" class="nav-link px-2 <?= active_link(['HomeController'], ['index']); ?>">Home</a></li>
      			<li class="nav-item dropdown">
          			<a class="nav-link dropdown-toggle <?= active_link(['DoctorProfileController'], ['index', 'create_profile', 'update_profile']); ?>" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">Manage Profile</a>
         				<ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
						 	<?php if (dr_profile(session()->get('id')) === false) : ?>
								<li><a class="dropdown-item <?= sub_active_link(['DoctorProfileController'], ['create_profile']); ?>" href="<?= base_url('doctor/Profile/create_profile'); ?>">Create Profile</a></li>
							<?php elseif (dr_profile(session()->get('id')) === true) : ?>
								<li><a class="dropdown-item <?= sub_active_link(['DoctorProfileController'], ['index', 'update_profile']); ?>" href="<?= base_url('doctor/Profile/view_profile'); ?>">View Profile</a></li>
							<?php endif; ?>
          				</ul>
        		</li>
				<li class="nav-item dropdown">
          			<a class="nav-link dropdown-toggle  text-white" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">Consultation</a>
         				<ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
							<li><a class="dropdown-item" href="<?= base_url('doctor/Consultation/PatientQueue'); ?>">Patient Queue</a></li>
          				</ul>
        		</li>
    
			<?php elseif (session()->has('id') && session()->get('role') == 3) : ?> <!-- Pharmacist -->
				<li><a href="<?= base_url('pharmacist/home'); ?>" class="nav-link px-2 <?= active_link(['HomeController'], ['index']); ?>">Home</a></li>
				<li><a href="<?= base_url('pharmacist/Medicine/list_of_medicine'); ?>" class="nav-link px-2 <?= active_link(['MedicineManagementController'], ['index', 'add_medicine', 'edit_medicine']); ?>">Medicine Management</a></li>
				<li><a href="<?= base_url('pharmacist/Order/list_of_order'); ?>" class="nav-link px-2 <?= active_link(['MedicineOrderController'], ['index', 'view_order_details']); ?>">Medicine Order</a></li>

			<?php elseif (session()->has('id') && session()->get('role') == 4) : ?> <!-- Patient -->
				<li><a href="<?= base_url('patient/home'); ?>" class="nav-link px-2 text-white active">Home</a></li>
				
			<?php else : ?>
				<li><a href="<?= base_url('/'); ?>" class="nav-link px-2 text-white active">Home</a></li>
				<li><a href="<?= base_url('about'); ?>" class="nav-link px-2 text-secondary">About</a></li>
			<?php endif; ?>
        </ul>

        <div class="text-end">
			<?php if (session()->has('id') && session()->get('role') == 1) : ?>
				<a href="<?= base_url('receptionist/logout'); ?>" type="button" class="btn btn-outline-danger me-2">Logout (<?= session()->get('email') ?>)</a>
			<?php elseif (session()->has('id') && session()->get('role') == 2) : ?>
				<a href="<?= base_url('doctor/logout'); ?>" type="button" class="btn btn-outline-danger me-2">Logout (<?= session()->get('email') ?>)</a>
			<?php elseif (session()->has('id') && session()->get('role') == 3) : ?>
				<a href="<?= base_url('pharmacist/logout'); ?>" type="button" class="btn btn-outline-danger me-2">Logout (<?= session()->get('email') ?>)</a>
			<?php elseif (session()->has('id') && session()->get('role') == 4) : ?>
				<a href="<?= base_url('patient/logout'); ?>" type="button" class="btn btn-outline-danger me-2">Logout (<?= session()->get('email') ?>)</a>
			<?php else : ?>
				<a href="<?= base_url('login'); ?>" type="button" class="btn btn-outline-primary me-2">Login</a>
				<a href="<?= base_url('register'); ?>" type="button" class="btn btn-outline-success me-2">Register</a>
			<?php endif; ?>
        </div>
      	</div>
    </div>
</header>
<body style="background-color:lightgrey;">
<!-- CONTENT -->

<div class="container">
	<br>
	<?php if ($title !== null) : ?>
		<h2 class="pb-2 border-bottom">
			<?= $title; ?>
		</h2>
	<?php endif ?>
	<?php if ($success !== null) : ?>
        <div class="alert alert-success" role="success">
                <?= esc($success); ?>
        </div>
    <?php endif ?>
    <?php if ($errors !== null && is_array($errors)) : ?>
        <div class="alert alert-danger" role="alert">
            <ul>
                <?php foreach ($errors as $error) : ?>
                    <li><?= esc($error); ?></li>
                <?php endforeach ?>
            </ul>
        </div>
    <?php endif ?>
	<?= $content; ?>
</div>

<br>

<!-- FOOTER: COPYRIGHTS -->

<footer  class="footer fixed-bottom mt-auto py-3 bg-dark">
	<div class="container">
		<span class="text-muted">&copy; <?= date('Y'); ?> Digital Healthcare Information System</span>
	</div>
</footer>

<!-- SCRIPTS -->

<script type="text/javascript" src="<?php echo base_url('js/jquery/jquery.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/bootstrap/bootstrap.bundle.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/select2/select2.full.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/data-tables/jquery.dataTables.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/data-tables/dataTables.bootstrap5.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/sweetalert2/sweetalert2.all.min.js'); ?>"></script>
<script>
$( document ).ready(function() {
	$('select').select2({
		width: 'resolve',
	});
	//  
	$('#data-table').DataTable();
	var element = document.getElementById("data-table");
	if (element !== null)
		element.classList.add("table", "table-dark", "table-striped", "table-hover", "table-bordered", "table-responsive", "text-center");

	// JavaScript for disabling form submissions if there are invalid fields
	(function () {
	'use strict'

	// Fetch all the forms we want to apply custom Bootstrap validation styles to
	var forms = document.querySelectorAll('.needs-validation')

	// Loop over them and prevent submission
	Array.prototype.slice.call(forms)
		.forEach(function (form) {
			form.addEventListener('submit', function (event) {
			if (!form.checkValidity()) {
				event.preventDefault()
				event.stopPropagation()
			}

			form.classList.add('was-validated')
			}, false)
		})
	})();

	$( "#pay-bill" ).click(function() {
		var url = $("#url-pay").val();
		Swal.fire(
			'Payment Successful!',
			'',
			'success'
		).then((result) => {
			if (result.isConfirmed) {
				window.location.href = url;
			}
		})
	});
	
});
</script>
<!-- -->

</body>
</html>
