<form action="<?= base_url('/register'); ?>" method="post" class="needs-validation" novalidate>
    <div class="mb-3">
        <label for="email" class="form-label">Email address</label>
        <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" required>
        <div class="invalid-feedback">
            Please enter a valid email address.
        </div>
    </div>
    <div class="mb-3">
        <label for="password" class="form-label">Password</label>
        <input type="password" name="password" class="form-control" id="password" required>
        <div class="invalid-feedback">
            Please enter your password.
        </div>
    </div>
    <div class="mb-3">
        <label for="role" class="font-weight-bold">Role</label>
        <select name="role" class="form-control" id="role" required>
            <option value="" selected disabled>Choose Role ...</option>
            <option value="1">Receptionist</option>
            <option value="2">Doctor</option>
            <option value="3">Pharmacist</option>
            <option value="4">Patient</option>
        </select>
        <div class="invalid-feedback">
            Please select your role.
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>