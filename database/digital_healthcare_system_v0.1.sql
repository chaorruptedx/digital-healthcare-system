-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 25, 2021 at 06:27 PM
-- Server version: 8.0.23
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `digital_healthcare_system`
--
CREATE DATABASE IF NOT EXISTS `digital_healthcare_system` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `digital_healthcare_system`;

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `id` int NOT NULL,
  `id_doctor` int NOT NULL,
  `id_patient` int NOT NULL,
  `id_receptionist` int NOT NULL,
  `id_medical_history` int NOT NULL,
  `date` datetime NOT NULL,
  `reason` varchar(100) DEFAULT NULL,
  `appointment_status` smallint NOT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Delete',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bill`
--

CREATE TABLE `bill` (
  `id` int NOT NULL,
  `id_patient` int NOT NULL,
  `id_receptionist` int NOT NULL,
  `consultation_fee` decimal(10,2) NOT NULL,
  `drug_fee` decimal(10,2) NOT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Delete',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lookup_medicine`
--

CREATE TABLE `lookup_medicine` (
  `id` int NOT NULL,
  `code` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `status` smallint NOT NULL COMMENT '1 = Active, 0 = Inactive, -1 = Delete',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lookup_state`
--

CREATE TABLE `lookup_state` (
  `id` int NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Delete',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `medical_history`
--

CREATE TABLE `medical_history` (
  `id` int NOT NULL,
  `id_patient` int NOT NULL,
  `id_doctor` int NOT NULL,
  `id_bill` int NOT NULL,
  `height` decimal(5,2) NOT NULL,
  `weight` decimal(5,2) NOT NULL,
  `blood_pressure` int NOT NULL,
  `notes` varchar(255) NOT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Delete',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patient_queue`
--

CREATE TABLE `patient_queue` (
  `id` int NOT NULL,
  `id_patient` int NOT NULL,
  `id_medical_history` int NOT NULL,
  `queue_status` smallint NOT NULL,
  `status` smallint NOT NULL COMMENT '1 = Active, 0 = Inactive, -1 = Delete',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_detail`
--

CREATE TABLE `personal_detail` (
  `id` int NOT NULL,
  `id_user` int NOT NULL,
  `id_state` int NOT NULL,
  `nric_no` bigint NOT NULL,
  `user_no` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gender` smallint NOT NULL COMMENT '1 = Male, 2 = Female',
  `address` varchar(255) NOT NULL,
  `tel_no` varchar(20) NOT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Delete',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pharmacy_medicine`
--

CREATE TABLE `pharmacy_medicine` (
  `id` int NOT NULL,
  `id_patient` int NOT NULL,
  `id_pharmacist` int NOT NULL,
  `id_medicine` int NOT NULL,
  `id_bill` int NOT NULL,
  `quantity` int NOT NULL,
  `medication_status` smallint NOT NULL,
  `status` smallint NOT NULL COMMENT '1 = Active, 0 = Inactive, 1- = Delete',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` char(32) NOT NULL,
  `role` smallint NOT NULL COMMENT '1 = Receptionist, 2 = Doctor, 3 = Pharmacist, 4 = Patient',
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Delete',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_appointment_id_doctor` (`id_doctor`),
  ADD KEY `fk_appointment_id_patient` (`id_patient`),
  ADD KEY `fk_appointment_id_medical_history` (`id_medical_history`),
  ADD KEY `fk_appointment_id_receptionist` (`id_receptionist`);

--
-- Indexes for table `bill`
--
ALTER TABLE `bill`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_bill_id_patient` (`id_patient`),
  ADD KEY `fk_bill_id_receptionist` (`id_receptionist`);

--
-- Indexes for table `lookup_medicine`
--
ALTER TABLE `lookup_medicine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lookup_state`
--
ALTER TABLE `lookup_state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medical_history`
--
ALTER TABLE `medical_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_medical_history_id_patient` (`id_patient`),
  ADD KEY `fk_medical_history_id_doctor` (`id_doctor`),
  ADD KEY `fk_medical_history_id_bill` (`id_bill`);

--
-- Indexes for table `patient_queue`
--
ALTER TABLE `patient_queue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_patient_queue_id_patient` (`id_patient`),
  ADD KEY `fk_patient_queue_id_medical_history` (`id_medical_history`);

--
-- Indexes for table `personal_detail`
--
ALTER TABLE `personal_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_personal_detail_id_user` (`id_user`),
  ADD KEY `fk_personal_detail_id_state` (`id_state`);

--
-- Indexes for table `pharmacy_medicine`
--
ALTER TABLE `pharmacy_medicine`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pharmacy_medicine_id_patient` (`id_patient`),
  ADD KEY `fk_pharmacy_medicine_id_pharmacist` (`id_pharmacist`),
  ADD KEY `fk_pharmacy_medicine_id_medicine` (`id_medicine`),
  ADD KEY `fk_pharmacy_medicine_id_bill` (`id_bill`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bill`
--
ALTER TABLE `bill`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lookup_medicine`
--
ALTER TABLE `lookup_medicine`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lookup_state`
--
ALTER TABLE `lookup_state`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `medical_history`
--
ALTER TABLE `medical_history`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `patient_queue`
--
ALTER TABLE `patient_queue`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `personal_detail`
--
ALTER TABLE `personal_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pharmacy_medicine`
--
ALTER TABLE `pharmacy_medicine`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `appointment`
--
ALTER TABLE `appointment`
  ADD CONSTRAINT `fk_appointment_id_doctor` FOREIGN KEY (`id_doctor`) REFERENCES `personal_detail` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_appointment_id_medical_history` FOREIGN KEY (`id_medical_history`) REFERENCES `medical_history` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_appointment_id_patient` FOREIGN KEY (`id_patient`) REFERENCES `personal_detail` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_appointment_id_receptionist` FOREIGN KEY (`id_receptionist`) REFERENCES `personal_detail` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `bill`
--
ALTER TABLE `bill`
  ADD CONSTRAINT `fk_bill_id_patient` FOREIGN KEY (`id_patient`) REFERENCES `personal_detail` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_bill_id_receptionist` FOREIGN KEY (`id_receptionist`) REFERENCES `personal_detail` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `medical_history`
--
ALTER TABLE `medical_history`
  ADD CONSTRAINT `fk_medical_history_id_bill` FOREIGN KEY (`id_bill`) REFERENCES `bill` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_medical_history_id_doctor` FOREIGN KEY (`id_doctor`) REFERENCES `personal_detail` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_medical_history_id_patient` FOREIGN KEY (`id_patient`) REFERENCES `personal_detail` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `patient_queue`
--
ALTER TABLE `patient_queue`
  ADD CONSTRAINT `fk_patient_queue_id_medical_history` FOREIGN KEY (`id_medical_history`) REFERENCES `medical_history` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_patient_queue_id_patient` FOREIGN KEY (`id_patient`) REFERENCES `personal_detail` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `personal_detail`
--
ALTER TABLE `personal_detail`
  ADD CONSTRAINT `fk_personal_detail_id_state` FOREIGN KEY (`id_state`) REFERENCES `lookup_state` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_personal_detail_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Constraints for table `pharmacy_medicine`
--
ALTER TABLE `pharmacy_medicine`
  ADD CONSTRAINT `fk_pharmacy_medicine_id_bill` FOREIGN KEY (`id_bill`) REFERENCES `bill` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_pharmacy_medicine_id_medicine` FOREIGN KEY (`id_medicine`) REFERENCES `lookup_medicine` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_pharmacy_medicine_id_patient` FOREIGN KEY (`id_patient`) REFERENCES `personal_detail` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_pharmacy_medicine_id_pharmacist` FOREIGN KEY (`id_pharmacist`) REFERENCES `personal_detail` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
